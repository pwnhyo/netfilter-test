CC=gcc

all: netfilter-test

netfilter-test: netfilter-test.c parse_header.h
	$(CC) -o netfilter-test netfilter-test.c -lnetfilter_queue

clean:
	rm -f netfilter-test